#include <stdio.h>

main() {
    printf("Size of long long: %x\n", sizeof(long long));
    printf("Size of long long*: %x\n", sizeof(long long *));
    printf("Size of short: %x\n", sizeof(short));
    printf("Size of short* : %x\n", sizeof(short*));
}
