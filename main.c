#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define NV 100

//void display(int *p_data, int nv);

void cfilter(int *p_data, int *p_filter, int nv);


int main() 
{
    int i, val;
    unsigned int *random = malloc(100 * sizeof(unsigned int));
    unsigned int *filtered = malloc(100 * sizeof(unsigned int));
    struct timeval ctv_start, ctv_stop, asmtv_start, asmtv_stop;

    int ret;

    long long secs, usecs;

    FILE *frand;
    FILE *ffilt;
    FILE *fasmfilt;
    frand = fopen("rand.txt", "w");
    ffilt = fopen("cfilt.txt", "w");
    fasmfilt = fopen("asmfilt.txt", "w");

    srand(time(NULL));
    // Initialise values
    for (i = 0; i < NV; i++) {
        random[i] = rand() % 100;
    }
    // 
    for(i = 0; i < NV; i++) {
        fprintf(frand,"%d\n",random[i]);
    }

    
    // Start timer
    ret = gettimeofday(&ctv_start, 0);

    for(i = 0; i < 100; i++) {
        // Run the simple C filter a few times
        cfilter(random, filtered, NV);
        cfilter(filtered, filtered, NV);
        cfilter(filtered, filtered, NV);
        
        
    }

    // Stop timer
    ret = gettimeofday(&ctv_stop, 0);

    secs = ctv_stop.tv_sec - ctv_start.tv_sec;
    usecs = 1000000*secs + ctv_stop.tv_usec - ctv_start.tv_usec;

    printf("\n*********************************************************\n");
    printf("Time taken for 100 x 3 runs of C filter: %10lld usecs\n", usecs);
    printf("*********************************************************\n");
    

    for(i = 0; i < NV; i++) {
        fprintf(ffilt,"%d\n",filtered[i]);
       // fprintf(ffilt,"%d\n",50);
    }
    
    printf("random (address): %x\n", (unsigned int)random);
    printf("random (value): %d\n", *random);
    printf("filtered (address): %x\n", (unsigned int)filtered);
    printf("filtered (value): %d\n", *filtered);
    printf("number of values: %d\n\n", NV);

    printf("number of elements left at the end: %d\n\n", (NV-1)%4); // should be 3 for 100


    // Start timer
    ret = gettimeofday(&ctv_start, 0);

    for(i = 0; i < 100; i++) {
    // We've already written the filtered information out to file, so we can
    // reuse the memory
    asm_filter(random, filtered, NV);
    asm_filter(filtered, filtered, NV);
    asm_filter(filtered, filtered, NV);
    }

    // Stop timer
    ret = gettimeofday(&ctv_stop, 0);

    secs = ctv_stop.tv_sec - ctv_start.tv_sec;
    usecs = 1000000*secs + ctv_stop.tv_usec - ctv_start.tv_usec;

    printf("*********************************************************\n");
    printf("Time taken for 100 x 3 runs of ASM filter: %10lld usecs\n", usecs);
    printf("*********************************************************\n");

    for(i = 0; i < NV; i++) {
    /*    printf("random value %d: %d \t", i, random[i]);
        printf("filtered value %d: %d \n", i, filtered[i]);
        */
        fprintf(fasmfilt,"%d\n", filtered[i]);
    }

    /*
    free(random);
    free(filtered);
    */



    fclose(frand);
    fclose(ffilt);
    fclose(fasmfilt);

//    asmfilter(random, filtered, NV);


    return 0;
}

void cfilter(int *p_data, int *p_filter, int nv) {
    int i;

    // First and last cases need special treatment
    p_filter[0] = (p_data[0] + p_data[1])/2;

    // normal stencil for middle elements
    for (i = 1; i < nv-1; i++) {
        p_filter[i] = (p_data[i-1] + 2*p_data[i] + p_data[i+1])/4;
    }

    // Again, a bodge for the boundary
    p_filter[nv-1] = (p_data[nv-2] + p_data[nv-1])/2;
}


/*
void display(int *p_data, int nv)
{
    FILE *fp;
    FILE *f;
    int i;
    f = fopen("data.txt", "w");
    fp = popen("gnuplot", "w");
    for(i = 0; i < nv; i++) {
        fprintf(f,"%d\n",p_data[i]);
    }
    i = fclose(f);
    fprintf(fp,"plot \'data.txt\' w l");


}
*/


