%include "asm_io.inc"

segment .data

segment .bss

segment .text
    global asm_filter

; void asm_filter(int *input, int* output, int dim)
;
; IA32 version of: 
; void cfilter(int *p_data, int *p_filter, int nv) {
;     int i;
;     p_filter[0] = (p_data[0] + p_data[1])/2;
;     for (i = 1; i < nv-1; i++) {
;         p_filter[i] = (p_data[i-1] + 2*p_data[i] + p_data[i+1])/4;
;     }
;     p_filter[nv-1] = (p_data[nv-2] + p_data[nv-1])/2;
; }

%define PARAMBYTES 12 ;
asm_filter:
    push    ebp
    mov     ebp, esp

;    call    sub_dump_regs

    ; I think eax, ecx, and edx are all caller-saved
    mov     ecx, [ebp + 0x8]    ; first param -> random data
    mov     edx, [ebp + 0xc]    ; second param -> filtered array
    mov     eax, [ebp + 0x10]   ; number of elements, NV - set the counter

;    dump_regs 1

    ; Code for a chunk of 4 averaged values
    mov     ebx, 0      ; Start our counter
    mov     eax, 24

    ; First point gets done separately
    ; Need a register. Try esi, not sure if it's caller-saved so back it up in case
point1:
    push    esi
    mov     esi, [ecx]
    add     esi, [ecx + 4]
    shr     esi, 1
    mov     [ecx], esi
    pop     esi

centralpts:

    movdqu  xmm0, [ecx + ebx*4] 
    movdqu  xmm1, [ecx + ebx*4 + 4] 
    movdqu  xmm2, [ecx + ebx*4 + 8] 

    paddd   xmm0, xmm2
    ; Could try shl xmm1 here then just add once..?
    paddd   xmm0, xmm1      
    paddd   xmm0, xmm1
    psrld   xmm0, 2

    movdqu  [edx + ebx*4 + 4], xmm0

    add     ebx, 4
    sub     eax, 1
    jnz     centralpts 

;    ; TODO Finish the first/last elements
;    ; Get our number of points off the stack again
;    mov     ebx, [ebp + 0x10]
;    sub     ebx, 1
;    and     ebx, 0x3
;    jz      last_one    ; Even if we finish 

lastpts:
    
;    sub
    
return: 
    pop     ebp
    ret




;    call    print_int   ; print eax
;    call    print_nl
;    mov     eax, ecx
;    call    print_hex   ; print ecx
;    call    print_nl
;    mov     eax, edx
;    call    print_hex   ; print edx
;    call    print_nl
;    call    print_nl

;    mov     eax, ecx
;    add     eax, ebx
;    add     eax, ebx
;    add     eax, ebx
;    add     eax, 4
;    call    print_hex
;    call    print_nl
