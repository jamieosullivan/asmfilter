asmfilter.git
=======================================================================================================

This is a small project (to start with) to practice assembly and explore the performance benefits of 
SIMD assembly over conventional C compilation. 

At the moment, the code consists of a routine in assembly to run a simple averaging filter over a 
1-dimensional input array of random integers and output the result to another array, using SIMD 
instructions on the XMM registers to reduce the number of calculations and load/store operations to
memory. 
In tests so far (versus a simple C comparison function), the speedup is greater than a factor of 3, 
which seems close enough to a theoretical maximum of 4 (we can fit 4 ints into each XMM, and carry 
out operations on those 4 values in one instruction cycle). 

TODO:

- Fix up the end elements, last few will always need to be done individually
- Having just signed integers is obviously just a starting point, will want to try with float/double 
  types
- Once I've done functions for a couple of different types, see if I can write a generic function in
  C, and investigate if/how this can be implemented in ASM.
- Explore a bit the differences between the gcc-generated code and the ASM code
- Explore the differences found when the -O flags are used to optimize the C routine (e.g. loop 
  unrolling)
- Add tests:
    - Compare ASM output data with C function output data (look into other ways to get external
      test data for both functions)


Possible expansions:

- 2-dimensional arrays?
- Implement other algorithms, or even abstract out the algorithm somehow if possible

Acknowledgments & References:
    - I make use of some of Dr Paul Carter's code to get register dumps for debugging
      see http://www.drpaulcarter.com/pcasm/index.php
